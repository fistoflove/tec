<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Scss;

include_once("fields.php");

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;



$context['departments'] =  Timber::get_terms( array(
    'taxonomy' => 'department',
    'hide_empty' => true,
));

Timber::render( 'template.twig', $context);