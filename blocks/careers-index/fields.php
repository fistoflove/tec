<?php

use IMSWP\Helper\Fields;
use Timber\Timber;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
    ]
);

add_action( 'wp_ajax_nopriv_get_jobs_ajax', 'get_jobs_ajax' );

add_action( 'wp_ajax_get_jobs_ajax', 'get_jobs_ajax' );

function get_jobs_ajax() {
    $departments = explode(',', $_REQUEST['departments']);

    $context = Timber::get_context();

    foreach($departments as $department) {
        $term = get_term_by('id', $department, 'department');
        $jobs = Timber::get_posts([
            'post_type' => 'job',
            'posts_per_page' => -1,
            'tax_query' => [
                [
                    'taxonomy' => 'department',
                    'field'    => 'term_id',
                    'terms'    => $department,
                ]
            ]
        ]);
        if( count( $jobs ) > 0 ) {
            $context['ajax_departments'][] = [ "title" => $term->name, "jobs" => $jobs ];
        }
    }

    Timber::render( 'careers-index-results-ajax.twig', $context );
    die();
}