<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Helper;

include_once("fields.php");

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

Helper::output_this_block_css('case-study-index');

Helper::output_this_block_css('pattern-hero-two');

$context['industries'] =  Timber::get_terms( array(
    'taxonomy' => 'Industry',
    'hide_empty' => true,
    'fields'    => 'all'
    
));

$context['case_studies'] =  Timber::get_posts( [
    'post_type' => 'case-study',
    'posts_per_page' => 9
] );

// Adds custom getCaseStudies function to twig's context
add_filter( 'timber/twig', function( $twig ) {
    $twig->addFunction( new Twig_Function( 'getCaseStudies', function( $industry ) {
        $case_studies = Timber::get_posts( [
            'post_type' => 'case-study',
            'posts_per_page' => -1,
            'tax_query' => [
                [
                    'taxonomy' => 'Industry',
                    'field'    => 'term_id',
                    'terms'    => $industry->id,
                ]
            ]
        ] );
        return $case_studies;
    } ) );
    return $twig;
} );

Timber::render( 'template.twig', $context);