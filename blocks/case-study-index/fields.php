<?php

use IMSWP\Helper\Fields;
use Timber\Timber;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
    ]
);

add_action( 'wp_ajax_nopriv_get_case_studies_ajax', 'get_case_studies_ajax' );

add_action( 'wp_ajax_get_case_studies_ajax', 'get_case_studies_ajax' );

function get_case_studies_ajax() {
    $industries = explode(',', $_REQUEST['industries']);

    $context = Timber::get_context();
    $context['ajax_case_studies'] = [];

    foreach($industries as $industry) {
        $term = get_term_by('id', $industry, 'Industry');
        $case_studies = Timber::get_posts([
            'post_type' => 'case-study',
            'posts_per_page' => -1,
            'tax_query' => [
                [
                    'taxonomy' => 'Industry',
                    'field'    => 'term_id',
                    'terms'    => $industry,
                ]
            ]
        ]);

        foreach($case_studies as $case_study) {
            array_push($context['ajax_case_studies'], $case_study);
        }
    }

    Timber::render( 'cs-index-results-ajax.twig', $context );
    die();
}