<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Image", "image"],
        ["Items", "repeater", [
            ["Title", "text"],
            ["Label", "text"]
        ]],
    ]
);