<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Content", "wysiwyg"],
        ["Items", "repeater", [
            ["Name", "text"],
            ["Title", "text"],
            ["Content", "text"]
        ]],
        ["Bottom", "wysiwyg"]
    ]
);