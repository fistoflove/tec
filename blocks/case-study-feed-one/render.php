<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Scss;

include_once("fields.php");

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

$context['related'] = Timber::get_posts( [
    'post_type' => 'case-study',
    'posts_per_page' => 3,
]);

Timber::render( 'template.twig', $context);