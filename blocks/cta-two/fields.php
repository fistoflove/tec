<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Image", "image"],
        ["Link", "link"],
        ["Text", "text"],
    ]
);