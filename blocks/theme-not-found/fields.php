<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Content",
    [
        ["Title", "text"],
        ["Image One", "image"],
        ["Image Two", "image"],
        ["Image Three", "image"],
        ["Image Four", "image"],
    ]
);