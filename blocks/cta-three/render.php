<?php

use Timber\Timber;
use Timber\Post;
use IMSWP\Helper\Helper;

$context = Timber::context();

$context['post'] = new Post(get_the_ID());

$context['block'] = $block;

$context['fields'] = get_fields();

$context['is_preview'] = $is_preview;

if(Helper::is_custom_post_type($context['post'])) {
    Helper::output_this_block_css('cta-three');
}

Timber::render( 'template.twig', $context);