<?php
/**
 * Child Starter functions and definitions
 *
 */

use Timber\Timber;

// WP Admin bar style fix
add_action('wp_head', function() {
	Timber::render( 'favicons.twig');
});

add_action('wp_head', function() {
	echo '<link rel="stylesheet" href="https://i.icomoon.io/public/32fdff22a3/TEC/style.css">';
	if(is_user_logged_in()) {
	  echo "<style>.header {top: var(--wp-admin--admin-bar--height) !important;}</style>";
	}
  });

  add_action('wp_head', function() {
	$user = wp_get_current_user();
	if($user->exists() && str_contains($user->user_email, '@imsmarketing.ie')) {
		?>
		<script>
		  window.markerConfig = {
			project: '6392cb55bd4941196275f57e', 
			source: 'snippet',
			reporter: {
				email: 'vini@imsmarketing.ie',
				fullName: 'IMS Marketing',
			},
		  };
		</script>
		
		<script>
			!function(e,r,a){if(!e.__Marker){e.__Marker={};var t=[],n={__cs:t};["show","hide","isVisible","capture","cancelCapture","unload","reload","isExtensionInstalled","setReporter","setCustomData","on","off"].forEach(function(e){n[e]=function(){var r=Array.prototype.slice.call(arguments);r.unshift(e),t.push(r)}}),e.Marker=n;var s=r.createElement("script");s.async=1,s.src="https://edge.marker.io/latest/shim.js";var i=r.getElementsByTagName("script")[0];i.parentNode.insertBefore(s,i)}}(window,document);
		</script>
		<?php
	}
  });

// Adds custom getJobs function to twig's context
add_filter( 'timber/twig', function( $twig ) {
    $twig->addFunction( new Twig_Function( 'getJobs', function( $department ) {
        $jobs = Timber::get_posts( [
            'post_type' => 'job',
            'posts_per_page' => -1,
            'tax_query' => [
                [
                    'taxonomy' => 'department',
                    'field'    => 'term_id',
                    'terms'    => $department->id,
                ]
            ]
        ] );
        return $jobs;
    } ) );
    return $twig;
} );
