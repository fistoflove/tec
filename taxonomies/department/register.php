<?php

$labels = array(
	'name'                       => _x( 'Departments', 'Taxonomy General Name', 'cec' ),
	'singular_name'              => _x( 'Department', 'Taxonomy Singular Name', 'cec' ),
	'menu_name'                  => __( 'Departments', 'cec' ),
	'all_items'                  => __( 'All Departments', 'cec' ),
	'parent_item'                => __( 'Parent Department', 'cec' ),
	'parent_item_colon'          => __( 'Parent Department:', 'cec' ),
	'new_item_name'              => __( 'New Department', 'cec' ),
	'add_new_item'               => __( 'Add New Department', 'cec' ),
	'edit_item'                  => __( 'Edit Department', 'cec' ),
	'update_item'                => __( 'Update Department', 'cec' ),
	'view_item'                  => __( 'View Department', 'cec' ),
	'separate_items_with_commas' => __( 'Separate departments with commas', 'cec' ),
	'add_or_remove_items'        => __( 'Add or remove Departments', 'cec' ),
	'choose_from_most_used'      => __( 'Choose from the most used departments', 'cec' ),
	'popular_items'              => __( 'Popular Departments', 'cec' ),
	'search_items'               => __( 'Search Departments', 'cec' ),
	'not_found'                  => __( 'Not Found', 'cec' ),
	'no_terms'                   => __( 'No departments', 'cec' ),
	'items_list'                 => __( 'Departments list', 'cec' ),
	'items_list_navigation'      => __( 'Departments list navigation', 'cec' ),
);
$rewrite = array(
	'slug'                       => 'department',
	'with_front'                 => false,
	'hierarchical'               => true,
);
$args = array(
	'labels'                     => $labels,
	'hierarchical'               => true,
	'public'                     => false,
	'show_ui'                    => true,
	'show_admin_column'          => true,
	'show_in_nav_menus'          => true,
	'show_tagcloud'              => true,
	'show_in_rest' 				 => true,
	'rewrite'                    => $rewrite,
);

register_taxonomy( 'department', array( 'job' ), $args );