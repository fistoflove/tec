<?php


register_taxonomy( 'industry', [ 'case-study' ], [
	'labels'                     => [
		'name'                       => _x( 'Industries', 'Taxonomy General Name', 'cec' ),
		'singular_name'              => _x( 'Industry', 'Taxonomy Singular Name', 'cec' ),
		'menu_name'                  => __( 'Industries', 'cec' ),
		'all_items'                  => __( 'All Industries', 'cec' ),
		'parent_item'                => __( 'Parent Industry', 'cec' ),
		'parent_item_colon'          => __( 'Parent Industry:', 'cec' ),
		'new_item_name'              => __( 'New Industry', 'cec' ),
		'add_new_item'               => __( 'Add New Industry', 'cec' ),
		'edit_item'                  => __( 'Edit Industry', 'cec' ),
		'update_item'                => __( 'Update Industry', 'cec' ),
		'view_item'                  => __( 'View Industry', 'cec' ),
		'separate_items_with_commas' => __( 'Separate Industries with commas', 'cec' ),
		'add_or_remove_items'        => __( 'Add or remove Industries', 'cec' ),
		'choose_from_most_used'      => __( 'Choose from the most used Industries', 'cec' ),
		'popular_items'              => __( 'Popular Industries', 'cec' ),
		'search_items'               => __( 'Search Industries', 'cec' ),
		'not_found'                  => __( 'Not Found', 'cec' ),
		'no_terms'                   => __( 'No Industries', 'cec' ),
		'items_list'                 => __( 'Industries list', 'cec' ),
		'items_list_navigation'      => __( 'Industries list navigation', 'cec' ),
	],
	'hierarchical'               => true,
	'public'                     => false,
	'show_ui'                    => true,
	'show_admin_column'          => true,
	'show_in_nav_menus'          => true,
	'show_tagcloud'              => true,
	'show_in_rest' 				 => true,
	'rewrite'                    => [
		'slug'                       => 'industry',
		'with_front'                 => false,
		'hierarchical'               => true,
	]
]);